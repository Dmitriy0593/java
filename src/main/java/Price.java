import java.util.Scanner;

public class Price {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Введите Код Продукта:");
        int code = in.nextInt();
        switch (code) {
            case 001:
                System.out.println("Бананы - 25.50грн");
                break;
            case 002:
                System.out.println("Апельсины - 37.50грн");
                break;
            case 003:
                System.out.println("Огурцы - 52грн");
                break;
            case 004:
                System.out.println("Картошка - 37.50грн");
                break;
            case 005:
                System.out.println("Ананас - 103грн");
                break;
            case 006:
                System.out.println("Киви - 51грн");
                break;
            default:
                System.out.println("Неверный код");
                break;
        }
    }
}
