public class Person{
    public static void main(String[] args) {

        TEST person1 = new TEST();
        person1.addParamName("Валерий");
        person1.addParamAge(18);
        TEST person2 = new TEST();
        person2.addParamName("Дмитрий");
        person2.addParamAge(45);

    }
}


class TEST {
    String name;
    int age;

    void addParamName(String username){
        name = username;
        System.out.println("Добрый день!" + " " + name);
    }
    void addParamAge(int pension){
        age = 65 - pension;
        System.out.println("Вам до пенсии" + " " + age + " Лет");
    }
}
