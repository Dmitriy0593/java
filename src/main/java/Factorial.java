import java.util.Scanner;



public class Factorial {

    public static void main (String[] args) {
        Scanner in = new Scanner(System.in);
        int value = in.nextInt();
        System.out.println(decision(value));
    }


    public static int decision(int num) {
        int fact = 1;

        for (int i = 1; i <= num; i++) {
            fact *= i;
        }

        return fact;
    }

}
