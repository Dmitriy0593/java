import java.util.Scanner;

public class operators {
    public static void main(String[] args) {
        Scanner tIn = new Scanner(System.in);
        System.out.print("Введите время:");
        int time = tIn.nextInt();
        Scanner mIn = new Scanner(System.in);
        System.out.print("Введите сколько у вас денег:");
        int money = mIn.nextInt();
        if (time >= 8 && time <= 12 && money > 10)
            System.out.println("Идем в магазин");
        else if (time > 12 && money > 50)
            System.out.println("Идем в кафе");
        else if (time <= 19 && money <= 50)
            System.out.println("Идем к соседу");
        else if (time > 19 && time <= 22)
            System.out.println("Смотрим телевизор");
        else if (time > 22)
            System.out.println("Идем спать");
        else
            System.out.println("Не знаю таких значений :'(");
    }
}

/*1. Создать программу которае решает проблему:
Если время от 8 до 12 и денег больше 10 то идем в магазин,
если время больше 12 и денег больше 50 идем в кафе
если денег меьше 50 и время меньше 19 то идем к соседу,
если  время больше 19 и меньше 22 то смотрим телевизор,
если больше 22 то идем спать
Результат выводим в консоль*/
